<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\MockWondeService;
use Tests\TestCase;
use App\Models\User;

class DashboardFeatureTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Test our dashboard returns a successful response
     *
     * @return void
     */
    public function test_the_dashboard_returns_a_successful_response()
    {
        $this->withoutExceptionHandling();
        $this->swap(WondeServiceInterface::class, new MockWondeService());
        $user = User::factory()->create([
            'wonde_id' => 'A1375078684'
        ]);
        $response = $this->actingAs($user)->get('/dashboard');

        $response->assertStatus(200);
    }

    /**
     * Test our dashboard contains the HTML element with `timetable` class
     *
     * @return void
     */
    public function test_the_dashboard_has_a_timetable()
    {
        $this->withoutExceptionHandling();
        $this->swap(WondeServiceInterface::class, new MockWondeService());
        $user = User::factory()->create([
            'wonde_id' => 'A1375078684'
        ]);
        $response = $this->actingAs($user)->get('/dashboard');
        $response->assertSee('class="timetable"', $escaped = false);
    }
}
