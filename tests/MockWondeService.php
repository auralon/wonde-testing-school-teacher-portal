<?php

namespace Tests;

use App\Contracts\WondeServiceInterface;

class MockWondeService implements WondeServiceInterface
{
    public $teacher;
    public $classes;

    public function __construct()
    {
        $this->teacher = (object) [
            'id' => 'A1375078684',
            'classes' => (object) [
                'data' => [
                    ['id' => 'A1022974129'],
                    ['id' => 'A494600076'],
                ],
            ]
        ];

        $this->classes = (object) [
            [
                'id' => 'A1022974129',
                'mis_id' => '13944',
                'name' => '10D/Ci',
                'description' => '10D/Ci',
                'students' => [],
                'lessons' => [],
            ],
            ['id' => 'A494600076'],
        ];
    }

    public function getTeacher(string $teacherId, array $includes = [], array $parameters = []): \stdClass
    {
        return $this->teacher;
    }

    public function getClass(string $classId, array $includes = [], array $parameters = []): \stdClass
    {
        return $this->classes;
    }
}
