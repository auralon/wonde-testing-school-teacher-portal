<?php

namespace Tests\Unit;

use Tests\TestCase;
use Mockery;
use Carbon\Carbon;
use App\Repository\ClassRepository;
use App\Repository\TeacherRepository;
use App\Services\DashboardService;

class DashboardServiceTest extends TestCase
{
    /**
     * Test DashboardService getData method
     *
     * @return void
     */
    public function test_get_data()
    {
        $response = $this->getData();
        $this->assertTrue(isset($response['data']['classes']));
    }

    /**
     * Test DashboardService getFormattedClassesData method
     */
    public function test_get_formatted_classes_data()
    {
        $response = $this->getData();
        $formattedData = $response['dashboardService']->getFormattedClassesData($response['data']);
        $this->assertTrue(is_array($formattedData));
    }

    /**
     * Handle mock for getData method
     *
     * @return array $data
     */
    private function getData(): array
    {
        $teacherWithClassesObject = (object) [
            'id' => 'A1375078684',
            'surname' => 'Abell',
            'forename' => 'Anita',
            'classes' => (object) [
                'data' => [
                    (object) ['id' => 'A494600076'],
                    (object) ['id' => 'A472765772'],
                ]
            ]
        ];

        $teacherRepositoryMock = Mockery::mock(TeacherRepository::class);
        $teacherRepositoryMock->expects('getById')
            ->shouldReceive(['A1375078684', ['classes']])
            ->andReturn([$teacherWithClassesObject]);

        $classIncludes = [
            'subject',
            'students',
            'lessons',
            'lessons.room',
            'lessons.period'
        ];

        $classParameters = [
            'has_students' => 1,
            'has_lessons' => 1,
        ];

        $classWithIncludesForParameters = [
            (object) [
                'id' => 'A494600076',
                'name' => '10y/Sc1',
                'students' => (object) [
                    'data' => [
                        (object) ['forename' => 'Test', 'surname' => 'Student 1'],
                        (object) ['forename' => 'Test', 'surname' => 'Student 2'],
                        (object) ['forename' => 'Test', 'surname' => 'Student 3'],
                    ]
                ],
                'lessons' => (object) [
                    'data' => [
                        (object) [
                            'start_at' => Carbon::now(),
                            'end_at' => Carbon::now()->addDays(1),
                            'room' => (object) [
                                'data' => (object) ['name' => 'Room 12345']
                            ]
                        ],
                    ]
                ]
            ],
            (object) [
                'id' => 'A472765772',
                'name' => '11yz/Sc1',
                'students' => (object) [
                    'data' => [
                        (object) ['forename' => 'Test', 'surname' => 'Student 1'],
                        (object) ['forename' => 'Test', 'surname' => 'Student 2'],
                        (object) ['forename' => 'Test', 'surname' => 'Student 3'],
                    ]
                ],
                'lessons' => (object) [
                    'data' => [
                        (object) [
                            'start_at' => Carbon::now(),
                            'end_at' => Carbon::now()->addDays(1),
                            'room' => (object) [
                                'data' => (object) ['name' => 'Room ABCDE']
                            ]
                        ],
                    ]
                ]
            ],
        ];

        $classRepositoryMock = Mockery::mock(ClassRepository::class);
        $classRepositoryMock->expects('getClassesForTeacher')
            ->shouldReceive([$teacherWithClassesObject, $classIncludes, $classParameters])
            ->andReturn([
                $classWithIncludesForParameters
            ]);

        $dashboardService = new DashboardService($teacherRepositoryMock, $classRepositoryMock);
        $data = $dashboardService->getData('A1375078684');

        return [
            'dashboardService' => $dashboardService,
            'data' => $data
        ];
    }
}
