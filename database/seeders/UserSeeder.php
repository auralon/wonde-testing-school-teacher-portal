<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Wonde\Client;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new Client(config('services.wonde.token'));
        $school = $client->school(config('services.wonde.school_id'));

        $includes = [
            'contact_details',
            'employment_details',
            'extended_details',
            'roles',
            'groups',
            'campus',
            'classes',
            'identifiers',
        ];

        $parameters = [
            'has_role' => 'Teacher'
        ];

        $now = Carbon::now();

        foreach ($school->employees->all($includes, $parameters) as $employee) {
            try {
                if ($employee?->contact_details?->data?->emails?->primary) {
                    User::firstOrCreate([
                        'email' => $employee->contact_details->data->emails->primary,
                    ],[
                        'wonde_id' => $employee->id,
                        'name' => ($employee->forename . ' ' . $employee->surname),
                        'email_verified_at' => $now,
                        'password' => app('hash')->make('password'),
                    ]);
                }
            } catch (\ErrorException $e) {
                echo $e->getMessage() . PHP_EOL;
            }
        }

    }
}
