<?php

namespace App\Http\Controllers;

use App\Services\DashboardService;

class DashboardController extends Controller
{
    /**
     * Get dashboard resource (for logged in Teacher)
     *
     * @param \App\Services\DashboardService
     * @return \Illuminate\View\View
     */
    public function show(DashboardService $dashboardService): \Illuminate\View\View
    {
        return view('dashboard', $dashboardService->getData(auth()->user()->wonde_id));
    }
}
