<?php

namespace App\Services;

use App\Repository\TeacherRepositoryInterface;
use App\Repository\ClassRepositoryInterface;
use Carbon\Carbon;

class DashboardService
{
    /**
     * Constructor method
     *
     * @param \App\Repository\TeacherRepositoryInterface $teacherRepository
     * @param \App\Repository\ClassRepositoryInterface $classRepository
     *
     * @return void
     */
    public function __construct(
        public TeacherRepositoryInterface $teacherRepository,
        public ClassRepositoryInterface $classRepository
    ) {
    }

    /**
     * Get data for logged in Teacher's dashboard
     *
     * @param string $teacherId
     *
     * @throws \Wonde\Exceptions\InvalidTokenException
     * @return array
     */
    public function getData(string $teacherId): array
    {
        $teacherIncludes = [
            'classes',
        ];
        $teacherWithClasses = $this->teacherRepository->getById($teacherId, $teacherIncludes);

        // prepare our class arguments, for use in loop later
        $classIncludes = [
            'subject',
            'students',
            'lessons',
            'lessons.room',
            'lessons.period'
        ];

        $classParameters = [
            'has_students' => 1,
            'has_lessons' => 1,
        ];

        $teachersClasses = $this->classRepository->getClassesForTeacher($teacherWithClasses, $classIncludes, $classParameters);

        $formattedTeachersClasses = $this->getFormattedClassesData($teachersClasses);

        return ['classes' => $formattedTeachersClasses];
    }

    /**
     * Format class data for each class associated to the teacher, including a list of students
     *
     * @param array $teachersClasses
     *
     * @return array $formattedTeachersClasses
     */
    public function getFormattedClassesData(array $teachersClasses): array
    {
        $formattedTeachersClasses = [];

        foreach ($teachersClasses as $class) {
            if (!isset($class->lessons->data[0])) {
                continue;
            }

            $classStartAtDateTime = new Carbon($class->lessons->data[0]->start_at->date);
            $classEndAtDateTime   = new Carbon($class->lessons->data[0]->end_at->date);

            // define key within multi-dimensional array as period, for frontend
            $key = $classStartAtDateTime->format('H:i') . ' - ' . $classEndAtDateTime->format('H:i');

            $formattedTeachersClasses[$classStartAtDateTime->format('Y-m-d')][$key] = $class;
        }

        ksort($formattedTeachersClasses);

        return $formattedTeachersClasses;
    }
}
