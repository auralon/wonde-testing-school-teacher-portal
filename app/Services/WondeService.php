<?php

namespace App\Services;

use App\Contracts\WondeServiceInterface;
use Carbon\Carbon;
use Wonde\Client;

class WondeService implements WondeServiceInterface
{
    /**
     * @var \Wonde\Endpoints\Schools
     */
    public $school;

    /**
     * Constructor method
     *
     * @param \Wonde\Client $client
     * @return void
     */
    public function __construct(public Client $client)
    {
        $this->school = $this->client->school(config('services.wonde.school_id'));
    }

    /**
     * Get teacher object from Wonde API
     *
     * @param string $teacherId
     * @param array $includes
     * @param array $parameters
     *
     * @return \stdClass
     */
    public function getTeacher(string $teacherId, array $includes = [], array $parameters = []): \stdClass
    {
        try {
            $teacher = $this->school->employees->get($teacherId, $includes, $parameters);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return (object) $e->getMessage();
        }

        return $teacher;
    }

    /**
     * Get class object from Wonde API
     *
     * @param string $classId
     * @param array $includes
     * @param array $parameters
     *
     * @return \stdClass
     */
    public function getClass(string $classId, array $includes = [], array $parameters = []): \stdClass
    {
        try {
            $class = $this->school->classes->get($classId, $includes, $parameters);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return (object) $e->getMessage();
        }

        return $class;
    }
}
