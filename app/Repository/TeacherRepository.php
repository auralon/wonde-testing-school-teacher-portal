<?php

namespace App\Repository;

use App\Contracts\WondeServiceInterface;
use App\Repository\TeacherRepositoryInterface;

class TeacherRepository implements TeacherRepositoryInterface
{
    /**
     * Constructor method
     *
     * @param \App\Contracts\WondeServiceInterface $wonde
     *
     * @return void
     */
    public function __construct(
        public WondeServiceInterface $wonde
    ) {
    }

    /**
     * Get teacher by ID
     *
     * @param string $teacherId
     * @param array $includes
     * @param array $parameters
     *
     * @return \stdClass $classes
     */
    public function getById(string $teacherId, array $includes = [], array $parameters = []): \stdClass
    {
        return $this->wonde->getTeacher($teacherId, $includes, $parameters);
    }
}
