<?php

namespace App\Repository;

interface ClassRepositoryInterface
{
    /**
     * Get classes for a specific teacher
     *
     * @param \stdClass $teacherWithClasses
     * @param array $includes
     * @param array $parameters
     *
     * @return array $classes
     */
    public function getClassesForTeacher(\stdClass $teacherWithClasses, array $includes = [], array $parameters = []);
}
