<?php

namespace App\Repository;

interface TeacherRepositoryInterface
{
    public function getById(string $teacherId, array $includes, array $parameters);
}
