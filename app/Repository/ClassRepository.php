<?php

namespace App\Repository;

use App\Contracts\WondeServiceInterface;
use App\Repository\ClassRepositoryInterface;

class ClassRepository implements ClassRepositoryInterface
{
    /**
     * Constructor method
     *
     * @param \App\Contracts\WondeServiceInterface $wonde
     *
     * @return void
     */
    public function __construct(
        public WondeServiceInterface $wonde
    ) {
    }

    /**
     * Get classes for a specific teacher
     *
     * @param \stdClass $teacherWithClasses
     * @param array $includes
     * @param array $parameters
     *
     * @return array $classes
     */
    public function getClassesForTeacher(\stdClass $teacherWithClasses, array $includes = [], array $parameters = []): array
    {
        $classes = [];

        foreach ($teacherWithClasses->classes->data as $class) {
            $classes[] = $this->wonde->getClass($class->id, $includes, $parameters);
        }

        return $classes;
    }
}
