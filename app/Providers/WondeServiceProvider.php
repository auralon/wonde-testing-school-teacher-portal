<?php

namespace App\Providers;

use App\Services\WondeService;
use Illuminate\Support\ServiceProvider;
use Wonde\Client;

class WondeServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap Wonde service.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(\App\Contracts\WondeServiceInterface::class, function () {
            $client = new Client(config('services.wonde.token'));
            return new WondeService($client);
        });
    }
}
