<?php

namespace App\Contracts;

interface WondeServiceInterface
{
    /**
     * Get teacher object
     *
     * @param string $teacherId
     * @param array $includes
     * @param array $parameters
     *
     * @return \stdClass
     */
    public function getTeacher(string $teacherId, array $includes = [], array $parameters = []);

    /**
     * Get class object
     *
     * @param string $classId
     * @param array $includes
     * @param array $parameters
     *
     * @return \stdClass
     */
    public function getClass(string $classId, array $includes = [], array $parameters = []);

}
