# Wonde Testing School - Teacher Portal

Following the Wonde Developer Technical Exercise v1.1, this is a teacher's portal for the Test School.

## Setup

Setup our `.env` file:
`cp .env.example .env`

Edit the `.env` and provide the required values for the following variables:
```
WONDE_API_TOKEN=
WONDE_SCHOOL_ID=
```

Create our docker database entry point:
`touch db.sql`

Bring the site up using Docker:
`docker-compose up`

Install PHP dependencies:
`docker-compose exec php composer install`

Generate a Laravel key:
`docker-compose exec php php artisan key:generate`

At this point you will likely need to restart the docker containers for the `APP_KEY` env var change to take effect.

Run the Laravel migrations and seeders:
`docker-compose exec php php artisan migrate:fresh --seed`

**NOTE**: The seeders rely on API access for the purpose of this exercise.

Build the assets:
`npm install && npm run dev`

## Access the site

Navigate to https://localhost in your browser.

To login, use the following credentials:

* Username: `Abell@example.com`
* Password: `password`

## Tests

Run the test suite:
`docker-compose exec php php artisan test`

## Implementation Description

Every user in the system is a teacher in the Test School.

When a teacher logs in, they are presented with a dashboard that shows a timetable planner for the _current week only_ (see TODO below).

The planner shows a list of days where there are classes (with lessons) for the currently logged in teacher.

Each day has a list of lessons (sorted by period start), and for each lesson there is a list of associated students.

---
# DUE TO HALF-TERM, THE CURRENT DASHBOARD SHOWS NO DATA!

---

## Additional Technical Documentation

You can view the generated documentation [here](https://gl.githack.com/auralon/wonde-testing-school-teacher-portal/raw/master/docs/index.html)

### TODO
- [ ] Add date options to show class/student information for any given week, instead of just the current
- [ ] Allow registration, or remove registration test, depending on requirements (for 100% test pass in the suite)
- [ ] Fetch dashboard content via AJAX to speed up page load
