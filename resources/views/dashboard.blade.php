<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <ol class="timetable">
                        @foreach ($classes as $date => $timeframes)
                            <li class="day">
                                <span class="date block">{{ $date }}</span>
                                <ol>
                                    @foreach ($timeframes as $timeframe => $class)
                                        <li class="period">
                                            <span class="timeframe block"><label>TIME:</label> {{ $timeframe }}</span>
                                            <span class="room block"><label>ROOM:</label> {{ $class->lessons->data[0]->room->data->name }}</span>
                                            <span class="students block"><label>STUDENTS:</label>
                                                <ul>
                                                    @foreach ($class->students->data as $student)
                                                        <li class="student">{{ $student->forename }} {{ $student->surname }}</li>
                                                    @endforeach
                                                </ul>
                                            </span>
                                        </li>
                                    @endforeach
                                </ol>
                            </li>
                        @endforeach
                    </ol>
                    @if (count($classes) == 0)
                        <div class="notification">
                            <p>You have no classes for this week.</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
