<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Teacher Portal | Wonde Testing School</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.dark\:text-gray-500{--tw-text-opacity:1;color:#6b7280;color:rgba(107,114,128,var(--tw-text-opacity))}}
        </style>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased">
        <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
            @if (Route::has('login'))
                <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                    @auth
                        <a href="{{ url('/dashboard') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Dashboard</a>
                    @else
                        <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
                <div class="flex justify-center pt-8 sm:pt-0">
                    <svg width="131" height="32" viewBox="0 0 131 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M13.2991 0C8.77743 0 4.32222 0.666667 0 2V16.6667C0 24.3333 5.31965 29.4667 13.2991 32C21.2786 29.4667 26.5983 24.3333 26.5983 16.6667V2C22.276 0.666667 17.8208 0 13.2991 0ZM19.9487 3.33333V16L13.2991 12L6.64957 16V3.33333C8.64443 2.66667 13.2991 2.66667 13.2991 2.66667C13.2991 2.66667 17.9538 2.66667 19.9487 3.33333ZM13.2991 29.2C10.4398 28.2667 7.84649 26.6667 5.71863 24.5333L13.2991 19.9333L20.8796 24.5333C18.7518 26.6667 16.1584 28.2667 13.2991 29.2Z" fill="#4368FA"></path>
                        <path d="M54.9919 8.6665H60.112L54.9919 24.6665H50.5367L48.0098 16.1332L45.483 24.6665H41.0278L35.9741 8.6665H41.0278L43.3551 17.2665L45.8155 8.6665H50.2707L52.731 17.2665L54.9919 8.6665Z" fill="#27325E"></path>
                        <path d="M74.2092 22.6664C70.9509 25.9997 65.6312 25.9997 62.3064 22.733C60.7106 21.133 59.7796 18.933 59.8461 16.6664C59.8461 11.9997 63.5699 8.19971 68.2246 8.19971C72.8793 8.19971 76.6695 11.933 76.6695 16.5997V16.6664C76.6695 18.933 75.8051 21.0664 74.2092 22.6664ZM65.6312 19.3997C67.0941 20.7997 69.4215 20.7997 70.8179 19.3997C71.5493 18.6664 71.8818 17.6664 71.8818 16.6664C71.9483 15.6664 71.5493 14.6664 70.8179 13.933C69.355 12.533 67.0276 12.533 65.6312 13.933C64.8998 14.6664 64.5008 15.6664 64.5673 16.6664C64.5008 17.6664 64.8998 18.6664 65.6312 19.3997Z" fill="#27325E"></path>
                        <path d="M87.8407 8.19988C89.4366 8.13322 90.966 8.79988 92.0964 9.93322C93.2933 11.2665 93.8918 12.9999 93.8253 14.7999V24.6665H89.0376V15.5332C89.1041 14.7332 88.8381 13.9332 88.2397 13.3999C87.7077 12.8666 86.9763 12.5999 86.2448 12.6666C85.4469 12.5999 84.6489 12.9332 84.1169 13.4666C83.5185 14.1332 83.2525 15.0665 83.319 15.9332V24.6665H78.5313V8.66655H83.319V10.1999C84.2499 8.86655 85.7793 8.19988 87.8407 8.19988Z" fill="#27325E"></path>
                        <path d="M107.789 3.79993L112.577 2.2666V24.6666H107.789V23.1333C106.593 24.4666 104.864 25.1999 103.068 25.0666C101.007 25.0666 99.012 24.1999 97.6821 22.5999C96.2192 20.9333 95.4213 18.7999 95.4878 16.5999C95.4213 14.3999 96.2192 12.2666 97.6821 10.5999C99.012 8.99994 101.007 8.13327 103.068 8.13327C104.864 7.99993 106.593 8.73327 107.789 10.0666V3.79993ZM101.273 19.5333C102.802 20.9333 105.196 20.9333 106.726 19.5333C107.457 18.7999 107.856 17.7333 107.789 16.6666C107.856 15.5999 107.457 14.5999 106.726 13.7999C105.196 12.3999 102.802 12.3999 101.273 13.7999C100.541 14.5333 100.142 15.5999 100.209 16.6666C100.209 17.7333 100.541 18.7333 101.273 19.5333Z" fill="#27325E"></path>
                        <path d="M119.493 18.5999C120.025 20.0665 121.288 20.8665 123.283 20.8665C124.413 20.9332 125.544 20.4665 126.342 19.6665L130.132 21.8665C128.536 24.0665 126.275 25.1332 123.216 25.1332C120.823 25.2665 118.562 24.3999 116.833 22.7332C115.237 21.1332 114.372 18.9332 114.439 16.6665C114.306 12.1332 117.83 8.3332 122.352 8.19987C122.551 8.19987 122.684 8.19987 122.884 8.19987C125.078 8.1332 127.14 8.99987 128.669 10.5999C130.198 12.1999 131.063 14.3999 130.996 16.5999C130.996 17.2665 130.93 17.8665 130.797 18.5332L119.493 18.5999ZM119.426 14.9999H126.275C125.943 13.3999 124.48 12.3332 122.884 12.4665C121.022 12.4665 119.892 13.3332 119.426 14.9999Z" fill="#27325E"></path>
                    </svg>
                </div>
                <div class="flex justify-center pt-8 sm:pt-0">
                    <h1>Testing School - Teacher Portal</h1>
                </div>
            </div>
        </div>
    </body>
</html>
